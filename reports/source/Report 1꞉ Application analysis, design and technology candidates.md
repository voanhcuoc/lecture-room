---
title: 'Report 1: Application analysis, design and technology candidates'
created: '2020-04-10T03:55:32.825Z'
modified: '2020-04-12T01:37:27.639Z'
---

# Report 1: Application analysis, design and technology candidates

<div style="font-size: 0.85em">

Che Tiep Chan Khoa _(mailto:1611594@hcmut.edu.vn)_ <span style="float: right">Apr 11, 2020</span>
Bui Duc Phan _(mailto:1612507@hcmut.edu.vn)_

</div>
<br/>

<div style="font-size: 0.85em; padding-left: 10%; padding-right: 10%">

**Abstract.** This report document describes the desired functionality of a conferencing application for a remote lecture room. From the clarification given, requirements for underlying communication protocols and software stack will be inferred. Then technology candidates will be discussed and chosen. Finally, we will limit scope for implementation of a prototype for experimental purpose.
The application focuses on 2 core communicating features for users: screen-casting presentation and voice communication, in real time. Main concerns on functionality is: maintaining real time user experience; providing reliable fallbacks for bad network connections; and, to distinguish this application from existing solutions, enabling the lecturer to control presentation view shown to students - which may be composed of many screen-casting streams - and to impose a policy on how students can share their screen and communicate with each others; security aspects within internal user agents and with externel malicious actors.
After all, we decide to based our communication protocol on WebRTC and implement the application on the web platform.

</div>

## 1. Introduction

In the time of epidemic and natural disaster, remote communication is highly important. Many mainstream remote conferencing applications were deployed and constantly improved to meet today needs: Google Meet, Jitsi Meet, Zoom... They are, more precise, general-purpose video conferencing applications, which serve well popular use cases such as online meeting, online customer support, ...

However, they may not provide optimal experience and workflow for specific use cases, compared with offline communicating sessions. For instance, we will consider the use case of a remote lecture room: a student want to ask a question or present a solution with the context of his screen, so the lecturer may want to preview that student's screen, then he may place it alongside with his main lecture slides in the presentation view - the view that all students will look at; the lecturer may want students to get explicit permission from him before speaking publicly in the room; the lecturer may want to control whether students can communicate privately with each others; also, good application design tailored for this specific need may reduce bandwidth usage thus result in a better user experience.

In this report, we will describe systematically the desired functionality of typical conferencing application tailored for remote lecture room in _section 2_. Then we design the application based on its functionality in _section 3_. _Section 4_ will present and discuss concrete technology candidates. Finally, _section 5_ concludes our findings and draws expectation on the prototype we will implement.

## 2. Functionality analysis

### 2.1. Definition

We quote this definition from section 3.2 of RFC 4597:

> **Lecture Mode Conference** scenario enables a conference with a lecturer who
   presents a topic and can allow questions.  The lecturer needs to know
   who the participants are and needs to be able to give them the right
   to speak.
> In general, the lecturer is seen/heard by the conference participants
   and often shares a presentation or application with the other
   participants.
> A participant joining this type of conference can get the identity of
   the lecturer and often the identities of the audience participants. [1]

For the sake of clarity, we define these terms:

* _The application_ is the graphical application software run on computer of a specific participant, composed of 2 layers: the interface and the protocol.
* _A participant_ is a user using the application to attend a conference, or the application acting on behalf of that user.
* _The conferencing system_ is our realization of Lecture Mode Conference created by communication of the application instances run on computers of all participants.
* _A stream_ is a screen-casting video stream, a voice audio stream, or a binary stream mixing up both, coming from a specific participant.
* _A presentation view_ is a viewport composed of one or more streams from one or more participants.

### 2.2. Features

We take for granted the below feature descriptions:

* (F1) The conferencing system MUST streams lecturer's screen and voice to all students in real time.
* (F2) The conferencing system MUST allow students to share their screen and request attention from the lecturer.
* (F3) The application MUST allow lecturer to compose a presentation view and students to view it. (for example lecturer's problem and a student's solution, or 2 students' solutions to compare with each others)
* (F4) The conferencing system SHOULD allow lecturer to impose policy on whether participants can communicate with each others privately (allow, allow with explicit permission from lecturer, not allow)
* (F5) The application SHOULD fallback to a reliable non-realtime view for users with bad network connections, because this view is more usable than a lag view or without a view.
* (F6) The application SHOULD provides familiar experiences that mimic an offline lecture room. However,
* (F7) The application SHOULD NOT provides experiences that can be considered anti-features or limitations of offline lecture room such as spatializing audio stream.
* (F8) The conferencing system MAY allow different authentication method: passphrase, organization's Single-Signed-On service, or IP authorization.
* (F9) The application MAY allow participants to compose their own view out of available video stream.
* (F10) The conferencing system MAY records the main presentation view to media server for archiving purpose.

Fig 1: Application GUI sketch

![](https://i.ibb.co/WVrDqbv/sketch.png)

## 3. Application design

We will try to clarify the technology requirements by separating mechanism from policy.

### 3.1. Mechanism

* (M1) _Direct peer-to-peer transports for all video and audio streams:_ to maximize real-time experience (F1).
* (M2) _Media stream synchronization method:_ to synchronize the playback of all streams consumed by one participant (F1).
* (M3) _Secure access control model, permission negotiating protocol:_ to enforce access policy (F4).
* (M4) _Out-of-band signalling channels:_ to exchange secret keys and negotiating permission (F2) (F3) (F4).
* (M5) _Secure tranport for stream:_ to secure from outside and enforce access policy at application protocol layer.
* (M6) _Media streaming server:_ for (F5), (F10)

### 3.2. Policy

In order to impose policy on the conferencing system, we have to utilize an access control model: **access matrix** [2] - a matrix with row and column are participants (row is stream producers and column is stream consumers), values are booleans or enum values of permission to their stream.

To enable access control policy, we define 2 roles out of participants:

* **Member**:
  * (P1) MAY encrypt their own streams, produce keys to decrypt and request, send their keys to host.
  * (P2) request permission to streams from host, request and decrypt streams for others then render them for user to look at, based on presentation view information.

* **Host** (aka conference administrator):
  * (P3) maintain a access matrix holding information about access control policy.
  * (P4) maintain a list of secret keys to authorize access to each stream.
  * (P5) maintain one or many presentation view composed of multiple stream.
  * (P6) answer to permission request from others, and give them keys to authorize stream they're allowed to access.
  * (P7) request participants to change secret keys if corresponding access is revoked.

Given a concrete scenario, in which lecturer is the host and students are members:

1. Lecturer create a conference room and give access to students. He block access of all streams except himself by default.
2. Students attend.  
   _(their application will annouce himself, generate a key for each of their own stream and send to lecturer, access matrix as in Fig 2)_
3. Students watch lecturer's presentation from lecturer's screen-casting and voice stream.  
   _(their application of course know the key to decrypt lecturer's stream, as in Fig 2)_
4. Student A want to ask a question about his solution and request attention from lecturer, lecturer may view his stream, then may incorporate his stream into the presentation view.  
   _(this time A's will be made accessible from all participants, as in Fig 3)_
5. All students see the new presentation view with A's stream inside.  
   _(their application receive new presentation view description, see A's stream in there so they request A's key from lecturer, then request stream from A then render the presentation view)_
6. Lecturer remove A's stream from the presentation view.  
   _(access to A's stream is revoked from all students, access matrix state return to Fig 2)_
7. Student B want to communicate with student C, so B request permission from C, C accept and send a request to lecturer. Lecturer accept then streams of B and C available for each others.  
   _(This way B and C are all accept to share their streams, as in Fig 4)_

<!-- to new page -->
<br/>
<br/>
<br/>
<br/>

```
Fig 2:         Fig 3:         Fig 4:

  H A B C        H A B C        H A B C
H _ x x x      H _ x x x      H _ x x x
A 0 _ 0 0      A x _ x x      A 0 _ 0 0
B 0 0 _ 0      B 0 0 _ 0      B 0 0 _ x
C 0 0 0 _      C 0 0 0 _      C 0 0 x _
```

### 3.3. User experience

The application should provide sane default options of access matrix (as in Fig 5) and operations to modify the access matrix in an intuitive manner. Each participant should be able to toggle sharing of their stream himself, alongside with access matrix maintained by the host.

Fig 5: Create room dialog

![](https://i.ibb.co/k510hz8/sketch-Page-2.png)

## 4. Technology candidates

```
Fig 6: WebRTC protocol stack

/-----------------\
|      WebRTC     | <- application layer
|   SRTP / SCTP   | <- secure presentation layer
|      DTLS       | <- secure transport layer
| ICE, STUN, TURN | <-| network
|       IP        | <-| layer
\-----------------/

```

**Web Real-Time Communication (WebRTC)** provide ubiquitous peer-to-peer tranports for real-time application [3]. It was widely-used conferencing application like Google Hangouts and Jitsi Meet. So it's perfectly suitable for (M1).

However, WebRTC don't allow access to underlying RTP timestamps [3] so (M2) isn't available by default. We can either design a new synchronization method, or use RTP directly, or mix up video and audio stream from one node and give up synchronizing streams from different participants. **Real-time Transport Protocol (RTP)** is a lower abstraction layer of WebRTC [4], so it should be harder to implement our application on.

WebRTC, as its name sounds, is a web standard and is implemented in web browsers as a Javascript API [3] by default. We can find out easier-to-use wrapper libraries like **PeerJS** [5], so implementing the application interface on web platform is the easiest way.

Web platform provide a well-established mechanism to access microphone audio stream and screen-casting video stream, in which user can select whether he want to share entire screen or an application window: the `getUserMedia` API [6].

Web platform ecosystem also has widely-used video and audio playback such as **VideoJS** [7] and **HowlerJS** [8].

Web platform has massive portability too. We can wrap our web application in **Electron** [9] or **QT Web Engine** [10] to provide a desktop application that is portable to many platforms.

Separate, out-of-band peer connections will be established to allow (M3), (M4). We still have to design and implement our specific permission negotiating protocol.

WebRTC operates on **Datagram Transport
Layer Security (DTLS)** secure transport [11], which fulfill (M5).

Finally, because **Jitsi Meet** is open source software [12], we may consider develop our application by hacking on its source.

## 5. Conclusion

Over previous sections, we have gone through the analysis and design processes of the application. We also choose technology candidates as dependencies to implement a prototype. What remains is implementation and experimenting processes, at this point we have picked a subset of features to implement a sufficient prototype for experimental purpose, then schedule them into 2-weeks stages:

* Stage 0: 1-to-1 screen-sharing to evaluate technologies
* Stage 1: (F1), (F2)
* Stage 2: (F3), (F4)
* Stage 3: (F5)
* Remain features can be included into discussion section of scientific paper

_Due to uncertainty on implementation process, we expect we can finish Stage 2 before project deadline, however Stage 1 may be enough for paper submission and Stage 0 fulfills the requirement of our course project._

## References

\[1]: Roni Even, Nermeen Ismail, "Conferencing Scenarios". [Online] Available: http://tools.ietf.org/html/rfc4597 [Accessed Apr 11, 2020]
\[2]: Sandhu, R. S., & Samarati, P., "Access control: principle and practice"
\[3]: Ian Hickson, Martin Thomson, Harald Alvestrand, Justin Uberti, Eric Rescorla, Peter Thatcher, Jan-Ivar Bruaroey and Peter Saint-Andre, "WebRTC 1.0: Real-time Communication Between Browsers". [Online] Available: http://www.w3.org/TR/webrtc/ [Accessed Apr 11, 2020]
\[4]: Colin Perkins, Magnus Westerlund
, Joerg Ott, "Web Real-Time Communication (WebRTC): Media Transport and Use of RTP". [Online] Available: http://tools.ietf.org/html/draft-ietf-rtcweb-rtp-usage-26 [Accessed Apr 11, 2020]
\[5]: Michelle Bu, Eric Zhang, "PeerJS - Simple peer-to-peer with WebRTC". [Software] [Online] Available: http://peerjs.com/ [Accessed Apr 11, 2020]
\[6]: Jim Barnett, Harald Alvestrand, Travis Leithead, Josh Soref, Martin Thomson, Jan-Ivar Bruaroey, Peter Thatcher, Dominique Hazaël-Massieux, and Stefan Håkansson: "Media Capture and Streams". [Online] Available: http://www.w3.org/TR/mediacapture-streams/ [Accessed Apr 11, 2020]
\[7]: Brightcove, Inc., "Video.js - open source HTML5 & Flash video player". [GitHub repository] [Online] Available: http://github.com/videojs/video.js)
\[8]: James Simpson and GoldFire Studios, Inc., "Javascript audio library for the modern web.". [GitHub repository] [Online] Available: http://github.com/goldfire/howler.js [Accessed Apr 11, 2020]
\[9]: Kitti Kredpattanakul, Yachai Limpiyakorn, "Transforming JavaScript-Based Web Application to Cross-Platform Desktop with Electron"
\[10]: The Qt Company Ltd., "Qt WebEngine Overview". [Documentation] [Online] Available: http://doc.qt.io/qt-5/qtwebengine-overview.html [Accessed Apr 11, 2020]
\[11]: Michael Tuexen, Randall R. Stewart, Randell Jesup, Salvatore Loreto, "Datagram Transport Layer Security (DTLS) Encapsulation of SCTP Packets". [Online] Available: http://tools.ietf.org/html/rfc8261 [Accessed Apr 11, 2020]
\[12]: Philipp Hancke, ESTOS GmbH, BlueJimp SARL, "Jitsi Meet - Secure, Simple and Scalable Video Conferences that you use as a standalone app or embed in your web application.". [GitHub repository] [Online] Available: http://github.com/jitsi/jitsi-meet [Accessed Apr 11, 2020]
